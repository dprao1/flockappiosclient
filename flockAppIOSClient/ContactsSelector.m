//
//  ContactsSelector.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactsSelector.h"
#import "Contacts.h"
#import <Foundation/NSString.h>


@interface ContactsSelector()
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UISegmentedControl *contactSegmentOutlet;
@property (retain, nonatomic) NSMutableArray *contactDictionaryInfoArray;
@property (retain, nonatomic) NSMutableDictionary *selectedContacts;
@property (nonatomic, retain) NSMutableArray *arrayLetters;
@end

@implementation ContactsSelector
@synthesize tableView;
@synthesize contactSegmentOutlet;
@synthesize delegate = _delegate;
@synthesize contactDictionaryInfoArray = _contactDictionaryInfoArray;
@synthesize selectedContacts = _selectedContacts;
@synthesize arrayLetters = _arrayLetters;


+ (NSArray *)englishAlphabet
{
    NSArray *letters = [[NSArray alloc] initWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    
    NSArray *aux = [NSArray arrayWithArray:letters];
    return aux;    
}

- (NSMutableArray *)createListWith: (NSArray *)myArray
{
    NSMutableArray *list = [[NSMutableArray alloc] initWithArray:myArray];
    [list addObject:@"#"];
    
    NSMutableArray *aux = [NSMutableArray arrayWithArray:list];
    return aux;
}

- (BOOL)isLetter: (NSString *)letter
{
	NSArray *letters = [[self class] englishAlphabet]; 
	BOOL isLetter = NO;
	
	for (int i = 0; i < [letters count]; i++)
	{
		if ([[[letter substringToIndex:1] uppercaseString] isEqualToString:[letters objectAtIndex:i]]) 
		{
			isLetter = YES;
			break;
		}
	}
	
	return isLetter;
}

- (BOOL)containsNullString: (NSString *)str
{
    //return ([[str lowercaseString] containsString:@"null"]) ? YES : NO;
    return NO;
}

-(NSMutableArray *)arrayLetters
{
    if (!_arrayLetters) {
        _arrayLetters = [self createListWith:[[self class]englishAlphabet]];
    }
    return _arrayLetters;
}


- (NSMutableDictionary *) selectedContacts
{
    if (!_selectedContacts) {
        _selectedContacts = [NSMutableDictionary new];
    }
    
    return _selectedContacts;
}

- (IBAction)SegmentValueChanged:(id)sender 
{

}

- (IBAction)CancelButtonPressed:(id)sender 
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)DoneButtonPressed:(id)sender 
{
    NSMutableArray *contactsSelected = [NSMutableArray new];
    
	for (int i = 0; i < [self.arrayLetters count]; i++)
	{
		NSMutableArray *obj = [[self.contactDictionaryInfoArray objectAtIndex:0] valueForKey:[self.arrayLetters objectAtIndex:i]];
		
		for (int x = 0; x < [obj count]; x++)
		{
			NSMutableDictionary *item = (NSMutableDictionary *)[obj objectAtIndex:x];
			BOOL checked = [[item objectForKey:@"checked"] boolValue];
            
			if (checked)
			{
                NSMutableDictionary *contactsData = [NSMutableDictionary new];

                [contactsData setValue:[item valueForKey:@"firstName"] forKey:@"firstName"];
                [contactsData setValue:[item valueForKey:@"email"] forKey:@"email"];
                
                [contactsSelected addObject:contactsData];
			}
		}
	}
    
    if ([self.delegate respondsToSelector:@selector(doneSelectingWithNumContacts:AndContacts:)]) {
        [self.delegate doneSelectingWithNumContacts:[contactsSelected count] AndContacts:contactsSelected];
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.contactSegmentOutlet setTitle:@"Address Book" forSegmentAtIndex:0];
    [self.contactSegmentOutlet setTitle:@"FaceBook" forSegmentAtIndex:1];
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
	CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
	CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    NSMutableArray *contactsInfoArray = [NSMutableArray new];
    
	for (int i = 0; i < nPeople; i++)
	{
		ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
		ABMultiValueRef email = ABRecordCopyValue(person, kABPersonEmailProperty);
		NSArray *propertyArray = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(email);
		CFRelease(email);
        
		NSString *emailAddresses = @"";
        BOOL hasManyEmailAddresses = NO;
		for (int i = 0; i < [propertyArray count]; i++)
		{
			if (emailAddresses == @"") 
			{
				emailAddresses = [propertyArray objectAtIndex:i];
			}
			else 
			{
                hasManyEmailAddresses = YES;
				emailAddresses = [emailAddresses stringByAppendingString:[NSString stringWithFormat:@",%@", [propertyArray objectAtIndex:i]]];
			}
		}
                
		CFStringRef firstNameString = ABRecordCopyValue(person, kABPersonFirstNameProperty);
        CFStringRef lastNameString = ABRecordCopyValue(person, kABPersonLastNameProperty);
        
		NSString *firstName = (__bridge NSString *)firstNameString;
		NSString *lastName = (__bridge NSString *)lastNameString;
        
        if ((__bridge id)lastNameString != nil)
        {
            firstName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        }
        
        NSMutableDictionary *info = [NSMutableDictionary new];
        
        [info setValue:[NSString stringWithFormat:@"%@", [[firstName stringByReplacingOccurrencesOfString:@" " withString:@""] substringToIndex:1]] forKey:@"letter"];
        
        [info setValue:[NSString stringWithFormat:@"%@", firstName] forKey:@"firstName"];
        
        [info setValue:@"-1" forKey:@"rowSelected"];
        
        if (emailAddresses != @"")
		{
                [info setValue:[NSString stringWithFormat:@"%@", emailAddresses] forKey:@"email"];
                
                if (!hasManyEmailAddresses) 
                {
                    [info setValue:[NSString stringWithFormat:@"%@", emailAddresses] forKey:@"emailSelected"];
                }
                else
                {
                    [info setValue:@"" forKey:@"emailSelected"];
                }
        }
        
        
        [contactsInfoArray addObject:info];
        if (firstNameString) CFRelease(firstNameString);
        if (lastNameString) CFRelease(lastNameString);
	}
	
    
	CFRelease(allPeople);
	CFRelease(addressBook);
    
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName"
												  ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	NSArray *sortedContactsData = [contactsInfoArray sortedArrayUsingDescriptors:sortDescriptors];
    	
	NSMutableDictionary	*info = [NSMutableDictionary new];
	for (int i = 0; i < [self.arrayLetters count]; i++)
	{
		NSMutableArray *array = [NSMutableArray new];
		
		for (NSDictionary *dict in sortedContactsData)
		{
			NSString *firstName = [dict valueForKey:@"firstName"];
			firstName = [firstName stringByReplacingOccurrencesOfString:@" " withString:@""];
			
			if ([[[firstName substringToIndex:1] uppercaseString] isEqualToString:[self.arrayLetters objectAtIndex:i]]) 
			{
				[array addObject:dict];
			} 
		}
		
		[info setValue:array forKey:[self.arrayLetters objectAtIndex:i]];
	}
	
	for (int i = 0; i < [self.arrayLetters count]; i++)
	{
		NSMutableArray *array = [NSMutableArray new];
		
		for (NSDictionary *dict in sortedContactsData)
		{
			NSString *firstName = [dict valueForKey:@"firstName"];
			firstName = [firstName stringByReplacingOccurrencesOfString:@" " withString:@""];
			
			if ((![self isLetter:firstName]) && (![self containsNullString:firstName]))
			{
				[array addObject:dict];
			}
		}
		
		[info setValue:array forKey:@"#"];
	}
    
	self.contactDictionaryInfoArray = [[NSMutableArray alloc] initWithObjects:info, nil];
	self.tableView.editing = NO;
	[self.tableView reloadData];
    
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setContactSegmentOutlet:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.tableView];
	NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
	
	if (indexPath != nil)
	{
		[self tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *kCustomCellID = @"ContactCell";

	UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kCustomCellID];
	if (cell == nil)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCustomCellID];
	}
	
	NSMutableDictionary *item = nil;

    NSMutableArray *obj = [[self.contactDictionaryInfoArray objectAtIndex:0] valueForKey:[self.arrayLetters objectAtIndex:indexPath.section]];
		
    item = (NSMutableDictionary *)[obj objectAtIndex:indexPath.row];
    
	cell.textLabel.text = [item objectForKey:@"firstName"];
	cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
	[item setObject:cell forKey:@"Cell"];
	
	BOOL checked = [[item objectForKey:@"checked"] boolValue];
	UIImage *image = (checked) ? [UIImage imageNamed:@"checked.png"] : [UIImage imageNamed:@"unchecked.png"];
	
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
	button.frame = frame;
	
	[button setBackgroundImage:image forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
	cell.backgroundColor = [UIColor clearColor];
	cell.accessoryView = button;
	
	return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	
	int i = 0;
	NSString *sectionString = [self.arrayLetters objectAtIndex:section];
	
	NSArray *array = (NSArray *)[[self.contactDictionaryInfoArray objectAtIndex:0] valueForKey:sectionString];
    
	for (NSDictionary *dict in array)
	{
		NSString *name = [dict valueForKey:@"name"];
		name = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
		
		if (![self isLetter:name]) 
		{
			i++;
		}
		else
		{
			if ([[[name substringToIndex:1] uppercaseString] isEqualToString:[self.arrayLetters objectAtIndex:section]]) 
			{
				i++;
			}
		}
	}
	
	return i;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView 
{
	
    return self.arrayLetters;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	
    return [self.arrayLetters indexOfObject:title];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.arrayLetters count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{	
	return [self.arrayLetters objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *item = nil;
    NSMutableArray *obj = [[self.contactDictionaryInfoArray objectAtIndex:0] valueForKey:[self.arrayLetters objectAtIndex:indexPath.section]];
	
	item = (NSMutableDictionary *)[obj objectAtIndex:indexPath.row];
    
    BOOL checked = [[item objectForKey:@"checked"] boolValue];
    
    [item setObject:[NSNumber numberWithBool:!checked] forKey:@"checked"];
    
    UITableViewCell *cell = [item objectForKey:@"Cell"];
    UIButton *button = (UIButton *)cell.accessoryView;
    
    UIImage *newImage = (checked) ? [UIImage imageNamed:@"unchecked.png"] : [UIImage imageNamed:@"checked.png"];
    [button setBackgroundImage:newImage forState:UIControlStateNormal];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self tableView:self.tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
}

@end
