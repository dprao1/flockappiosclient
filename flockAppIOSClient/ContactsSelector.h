//
//  ContactsSelector.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>

@protocol ContactsSelectorDelegate <NSObject>
@required
- (void) doneSelectingWithNumContacts: (NSInteger)numContacts AndContacts: (NSArray *)data;
@end

@interface ContactsSelector : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
@private
    
    //NSMutableArray *contactsFirstNameArray;
}
@property (nonatomic, weak) id <ContactsSelectorDelegate> delegate;
@end
