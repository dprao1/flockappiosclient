//
//  GuestList+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestList+Helper.h"

@implementation GuestList (Helper)

+(RKManagedObjectMapping *)getGuestListClassMappingForObjectManager: (RKObjectManager *)objectManager
{
    RKManagedObjectMapping *guestListMapping = [RKManagedObjectMapping mappingForClass:[GuestList class]];
    
    [guestListMapping  mapKeyPath:@"eventResponse" toAttribute:@"eventResponse"];
    [guestListMapping mapKeyPath:@"userId" toAttribute:@"userId"];
    
    [[objectManager mappingProvider] addObjectMapping:guestListMapping];
    
    RKObjectMapping *serializationMapping = [guestListMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[GuestList class]];
    
    return guestListMapping;
}
@end
