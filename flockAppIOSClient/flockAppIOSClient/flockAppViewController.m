//
//  flockAppViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "flockAppViewController.h"
#import "userDefaults.h"
#import "Event+Helper.h"
#import "CreateDisplayEventController.h"
#import "flockAppConstants.h"

@interface flockAppViewController() 
@property (weak, nonatomic) IBOutlet UIBarButtonItem *LoginBarButtonOutlet;

@end

@implementation flockAppViewController
@synthesize LoginBarButtonOutlet = _LoginBarButtonOutlet;
@synthesize HomeNavigationBarOutlet = _HomeNavigationBarOutlet;

-(void) userLoggedIn:(id)sender withLoginName:(NSString *)userId   
        Password:(NSString *)password andAccountType:(NSString *)accountType
{
    self.HomeNavigationBarOutlet.title = userId;
    self.LoginBarButtonOutlet.title = @"Account";
    [userDefaults saveUserLoginName:userId];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    NSString *userId = [userDefaults getUserLoginName];
    
    [super viewDidLoad];
    if (userId) {
        self.HomeNavigationBarOutlet.title = userId;
        self.LoginBarButtonOutlet.title = @"Account";
    }
}

- (void)viewDidUnload
{
    [self setHomeNavigationBarOutlet:nil];
    [self setLoginBarButtonOutlet:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier hasPrefix:@"Login"]) {
        LoginViewController *loginPage = (LoginViewController *)segue.destinationViewController;
        
        //If user already logged in send the user ID and password so that only
        //Logout button can be shown.
        loginPage.userId = [userDefaults getUserLoginName];
        loginPage.password = @"abc";
        loginPage.delegate = self;
    
    } else if ([segue.identifier hasPrefix:@"FoodEventCreate"]) {
        CreateDisplayEventController *dvc = (CreateDisplayEventController *)segue.destinationViewController;
        dvc.eventType = FOOD_EVENT;
    
    } else if ([segue.identifier hasPrefix:@"MovieEventCreate"]) {
        CreateDisplayEventController *dvc = (CreateDisplayEventController *)segue.destinationViewController;
        dvc.eventType = MOVIE_EVENT;
    
    } else if ([segue.identifier hasPrefix:@"HangoutEventCreate"]) {
        CreateDisplayEventController *dvc = (CreateDisplayEventController *)segue.destinationViewController;
        dvc.eventType = HANGOUT_EVENT;
    
    } else if ([segue.identifier hasPrefix:@"SportsEventCreate"]) {
        CreateDisplayEventController *dvc = (CreateDisplayEventController *)segue.destinationViewController;
        dvc.eventType = SPORTS_EVENT;
    }
}

@end
