//
//  EventPollOptions.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventPoll;

@interface EventPollOptions : NSManagedObject

@property (nonatomic, retain) NSString * pollOption;
@property (nonatomic, retain) EventPoll *isPartOfEventPoll;

@end
