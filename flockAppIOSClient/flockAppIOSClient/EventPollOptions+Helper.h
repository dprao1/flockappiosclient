//
//  EventPollOptions+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventPollOptions.h"
#import "RestKit/RestKit.h"

@interface EventPollOptions (Helper)
+(RKManagedObjectMapping *)getEventPollOptionsClassMappingForObjectManager: (RKObjectManager *)objectManager;
@end
