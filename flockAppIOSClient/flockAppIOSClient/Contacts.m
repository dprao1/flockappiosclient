//
//  Contacts.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Contacts.h"
#import "Groups.h"


@implementation Contacts

@dynamic firstName;
@dynamic fromAccount;
@dynamic isNotUserCreated;
@dynamic lastName;
@dynamic phoneNumber;
@dynamic photo;
@dynamic userId;
@dynamic belongsToGroup;

- (void) setMyFirstName: (NSString *)firstName
{
    self.firstName = firstName;
}
@end
