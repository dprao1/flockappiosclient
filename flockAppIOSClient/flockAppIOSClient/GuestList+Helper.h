//
//  GuestList+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestList.h"
#import "RestKit/RestKit.h"

@interface GuestList (Helper)
+(RKManagedObjectMapping *)getGuestListClassMappingForObjectManager: (RKObjectManager *)objectManager;
@end
