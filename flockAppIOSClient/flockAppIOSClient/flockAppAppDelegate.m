//
//  flockAppAppDelegate.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "flockAppAppDelegate.h"
#import "flockAppConstants.h"
#import "User+Helper.h"
#import "Event+Helper.h"
#import "Messages+Helper.h"
#import "EventPoll+Helper.h"
#import "EventPollOptions+Helper.h"
#import "GuestList+Helper.h"
#import "Groups+Helper.h"

@interface flockAppAppDelegate ()
- (void)setupRestClient;
@end

@implementation flockAppAppDelegate

@synthesize window = _window;

- (void) setupRestClient
{
    RKObjectManager *objectManager = [RKObjectManager objectManagerWithBaseURL:FLOCK_APP_BASE_URL];

    //FIXME: Remove this later and put a per user DB.
    RKManagedObjectStore *objectStore = [RKManagedObjectStore objectStoreWithStoreFilename:@"temp.sqlite"];
    objectManager.objectStore = objectStore;
    
    [RKObjectManager setSharedManager:objectManager];
    objectManager.client.requestQueue.showsNetworkActivityIndicatorWhenBusy = YES;
    
    //User Class setup
    //RKManagedObjectMapping *userMapping = [User getUserClassMappingForObjectManager: objectManager];
    [User getUserClassMappingForObjectManager: objectManager];
    
    //Event Class setup
    RKManagedObjectMapping *eventMapping = [Event getEventClassMappingForObjectManager:objectManager];
    
    //GuestList Class setup
    RKManagedObjectMapping *guestMapping = [GuestList getGuestListClassMappingForObjectManager:objectManager];
    
    //EventPolling Class setup
    RKManagedObjectMapping *eventPollMapping = [EventPoll getEventPollClassMappingForObjectManager:objectManager];
    
    //PollChoices Class setup
    RKManagedObjectMapping *eventPollOptionsMapping = [EventPollOptions getEventPollOptionsClassMappingForObjectManager:objectManager];
    
    //Messages Class setup
    RKManagedObjectMapping *messagesMapping = [Messages getMessagesClassMappingForObjectManager:objectManager];
    
    [Groups getGroupsClassMappingForObjectManager:objectManager];
    
    //Relationship Mappings setup
    [eventMapping mapKeyPath:@"eventPolls" toRelationship:@"eventPolls" withMapping:eventPollMapping serialize:YES];
    [eventMapping mapKeyPath:@"messages" toRelationship:@"hasMessages" withMapping:messagesMapping serialize:YES];
    [eventMapping mapKeyPath:@"guests" toRelationship:@"EventGuests" withMapping:guestMapping serialize:YES];
    
    [eventPollMapping mapKeyPath:@"eventPollOptions" toRelationship:@"optionsMapping" withMapping:eventPollOptionsMapping serialize:YES];
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupRestClient];
    
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
