//
//  User+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "User+Helper.h"

@implementation User (Helper)

+(RKManagedObjectMapping *)getUserClassMappingForObjectManager:(RKObjectManager *)objectManager
{
    RKManagedObjectMapping *userMapping = [RKManagedObjectMapping mappingForClass:[User class]];
    
    [userMapping mapKeyPath:@"firstName"    toAttribute:@"firstName"];
    [userMapping mapKeyPath:@"lastName"     toAttribute:@"lastName"];
    [userMapping mapKeyPath:@"userId"       toAttribute:@"userId"];
    [userMapping mapKeyPath:@"phoneNumber"  toAttribute:@"phoneNumber"];
    [userMapping mapKeyPath:@"accountType"  toAttribute:@"accountType"];
    [userMapping mapKeyPath:@"DOB"          toAttribute:@"dateOfBirth"];
    [userMapping mapKeyPath:@"homeAddress"  toAttribute:@"homeAddress"];
    [userMapping mapKeyPath:@"password"     toAttribute:@"password"];
    
    userMapping.primaryKeyAttribute = @"userId";
    
    [objectManager.mappingProvider setMapping:userMapping forKeyPath:@"/user"];
    
    RKObjectMapping *serializationMapping = [userMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[User class]];
    
    [objectManager.router routeClass:[User class] toResourcePath:@"/user/:userId"];
    [objectManager.router routeClass:[User class] toResourcePath:@"/user" forMethod:RKRequestMethodPOST];
    
    return userMapping;
}
@end
