//
//  Event+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Event.h"
#import "RestKit/RestKit.h"

@interface Event (Helper)
+(RKManagedObjectMapping *)getEventClassMappingForObjectManager: (RKObjectManager *)objectManager;
@end
