//
//  Groups.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Groups.h"
#import "Contacts.h"


@implementation Groups

@dynamic groupId;
@dynamic groupName;
@dynamic groupContacts;

@end
