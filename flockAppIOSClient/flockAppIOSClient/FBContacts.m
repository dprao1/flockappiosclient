//
//  FBContacts.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FBContacts.h"


@implementation FBContacts

@dynamic firstName;
@dynamic lastName;
@dynamic userId;
@dynamic photo;

@end
