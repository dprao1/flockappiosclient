//
//  MyEventsTableViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyEventsTableViewController.h"
#import "Event+Helper.h"
#import "CreateDisplayEventController.h"

@interface MyEventsTableViewController()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)setupFetchedResultsController;
- (void)fetchMyEventsFromRemote;
@end

@implementation MyEventsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[RKClient sharedClient].requestQueue cancelRequestsWithDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"KIRAN: In the View will Appear!!!!!!!!");
    [super viewWillAppear:animated];
    self.debug = YES;
    if (self.fetchedResultsController == nil) {
        NSLog(@"Fetched Results Controller is Nill!!!!!");
        [self setupFetchedResultsController];
    }
    
    [self fetchMyEventsFromRemote];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.fetchedResultsController = nil;
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MyEventsTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {   
    //TODO
    Event  *event = (Event *)[self.fetchedResultsController objectAtIndexPath:indexPath];
	//cell.textLabel.text = @"something"; // [[aPost objectAtIndex:indexPath.row] title];
    cell.textLabel.text = [event eventName];
    
    NSString *subTitle = [event eventType];
    NSRange stringRange = {0, MIN([subTitle length], 40)};
    // adjust the range to include dependent chars
    stringRange = [subTitle rangeOfComposedCharacterSequencesForRange:stringRange];
    cell.detailTextLabel.text = [subTitle substringWithRange:stringRange];
}

- (void)setupFetchedResultsController // attaches an NSFetchRequest to this UITableViewController
{
    if (nil == self.fetchedResultsController) {               
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
        request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"eventName" 
                                                                                         ascending:YES 
                                                                                          selector:@selector(localizedCaseInsensitiveCompare:)]];
        
        // no predicate because we want ALL the Topics
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                            managedObjectContext: [[RKObjectManager
                                                                                                    sharedManager].objectStore managedObjectContext]
                                                                              sectionNameKeyPath:nil
                                                                                       cacheName:nil];
        
        
    }
}



- (void)fetchMyEventsFromRemote {
    // Load the object model via RestKit	
    
    NSLog(@"KIRAN: Fetching Events from Remote!!!!!!!!!!");
    RKObjectManager* objectManager = [RKObjectManager sharedManager];
    
    [objectManager loadObjectsAtResourcePath:@"/event/k.t@abc.com" delegate:self
                                       block:^(RKObjectLoader* loader) {
                                           loader.objectMapping = [objectManager.mappingProvider objectMappingForClass:[Event class]];
                                       }];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void) request:(RKRequest *)request didLoadResponse:(RKResponse *)response
{
    if ([request isPUT]) {
        NSLog(@"YAYYYY Posted the object successfully!!!! and got response");
    } else {
        NSLog(@"Got something other than PUT");
    }
    
    if ([response isOK]) {
        NSLog(@"Retrieved : %@", [response bodyAsString]);
    } else {
        NSLog(@"Response is not OK!!!!!");
    }
}

- (void) request:(RKRequest *)request didFailLoadWithError:(NSError *)error
{
    NSLog(@"Failed to load with ERrort: %@", error);
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObjects:(NSArray*)objects {
    
    
    NSLog(@"KIRAN: OK LOaded the Objects!!!!!!!!!!!!!!!!!!");
	[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastUpdatedAt"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	NSLog(@"Loaded topics: %@", objects.lastObject);
	//[self loadObjectsFromDataStore];
    [self performFetch];
	[self.tableView reloadData];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    /*
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                    message:[error localizedDescription] 
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
	[alert show];
     */
	NSLog(@"Hit error: %@", error);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    
    Event *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([segue.destinationViewController respondsToSelector:@selector(setEvent:)]) {
        [segue.destinationViewController performSelector:@selector(setEvent:) withObject:event];
        [segue.destinationViewController performSelector:@selector(setEventType:) withObject:event.eventType];
    }
}

@end
