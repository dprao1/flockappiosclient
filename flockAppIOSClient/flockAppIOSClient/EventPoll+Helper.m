//
//  EventPoll+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventPoll+Helper.h"

@implementation EventPoll (Helper)

+(RKManagedObjectMapping *)getEventPollClassMappingForObjectManager: (RKObjectManager *)objectManager
{
    RKManagedObjectMapping *eventPollMapping = [RKManagedObjectMapping mappingForClass:[EventPoll class]];
    
    [eventPollMapping mapKeyPath:@"isOpen"              toAttribute:@"isOpen"];
    [eventPollMapping mapKeyPath:@"pollDescription"     toAttribute:@"pollDescription"];
    [eventPollMapping mapKeyPath:@"pollChosenOption"    toAttribute:@"pollChosenOption"];
        
    [[objectManager mappingProvider] addObjectMapping:eventPollMapping];
    
    RKObjectMapping *serializationMapping = [eventPollMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[EventPoll class]];
        
    return eventPollMapping;
}
@end
