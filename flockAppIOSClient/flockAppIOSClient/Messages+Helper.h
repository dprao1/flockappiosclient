//
//  Messages+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Messages.h"
#import "RestKit/RestKit.h"

@interface Messages (Helper)
+(RKManagedObjectMapping *)getMessagesClassMappingForObjectManager: (RKObjectManager *)objectManager;
@end
