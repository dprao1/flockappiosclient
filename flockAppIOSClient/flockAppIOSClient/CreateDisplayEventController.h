//
//  CreateFoodEventController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event+Helper.h"

@interface CreateDisplayEventController : UIViewController <UITextFieldDelegate>
@property (nonatomic, copy) NSNumber *eventId;
@property (nonatomic, copy) NSString *eventType;
@property (nonatomic, strong) Event    *event;
@end
