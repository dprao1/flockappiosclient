//
//  Groups.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Contacts;

@interface Groups : NSManagedObject

@property (nonatomic, retain) NSNumber * groupId;
@property (nonatomic, retain) NSString * groupName;
@property (nonatomic, retain) NSSet *groupContacts;
@end

@interface Groups (CoreDataGeneratedAccessors)

- (void)addGroupContactsObject:(Contacts *)value;
- (void)removeGroupContactsObject:(Contacts *)value;
- (void)addGroupContacts:(NSSet *)values;
- (void)removeGroupContacts:(NSSet *)values;

@end
