//
//  Groups+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Groups.h"
#import "RestKit/RestKit.h"

@interface Groups (Helper)
+(RKManagedObjectMapping *)getGroupsClassMappingForObjectManager:(RKObjectManager *)objectManager;
@end
