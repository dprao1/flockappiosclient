//
//  EventPoll.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event, EventPollOptions;

@interface EventPoll : NSManagedObject

@property (nonatomic, retain) NSString * chosenOption;
@property (nonatomic, retain) NSNumber * isOpen;
@property (nonatomic, retain) NSString * pollDescription;
@property (nonatomic, retain) Event *belongsToEvent;
@property (nonatomic, retain) NSSet *optionsMapping;
@end

@interface EventPoll (CoreDataGeneratedAccessors)

- (void)addOptionsMappingObject:(EventPollOptions *)value;
- (void)removeOptionsMappingObject:(EventPollOptions *)value;
- (void)addOptionsMapping:(NSSet *)values;
- (void)removeOptionsMapping:(NSSet *)values;

@end
