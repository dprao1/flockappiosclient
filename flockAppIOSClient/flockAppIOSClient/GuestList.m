//
//  GuestList.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuestList.h"
#import "Event.h"


@implementation GuestList

@dynamic eventResponse;
@dynamic userId;
@dynamic firstName;
@dynamic lastName;
@dynamic pollChoice;
@dynamic GuestListBelongsTo;

@end
