//
//  EventPollOptions+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventPollOptions+Helper.h"

@implementation EventPollOptions (Helper)

+(RKManagedObjectMapping *)getEventPollOptionsClassMappingForObjectManager: (RKObjectManager *)objectManager
{
    RKManagedObjectMapping *eventPollOptionsMapping = [RKManagedObjectMapping mappingForClass:[EventPollOptions class]];
    
    [eventPollOptionsMapping mapKeyPath:@"pollOption" toAttribute:@"pollOption"];
    
    [[objectManager mappingProvider] addObjectMapping:eventPollOptionsMapping];
    
    RKObjectMapping *serializationMapping = [eventPollOptionsMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[EventPollOptions class]];
    
    return eventPollOptionsMapping;
}
@end
