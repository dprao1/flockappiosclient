//
//  DisplayEventController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DisplayEventController.h"
#import "ActionSheetDatePicker.h"
#import "NSDate+TCUtils.h"
#import "userDefaults.h"
#import "chatMessageViewController.h"
#import "RestKit/RestKit.h"

#define  NUM_SECTIONS         6

#define  EVENT_INFO_SECTION_HEADER        @"Event Info"
#define  EVENT_RESPONSE_SECTION_HEADER    @"Your response"
#define  EVENT_POLL_SECTION_HEADER        @"Poll"
#define  EVENT_MESSAGES_SECTION_HEADER    @"Messages"
#define  EVENT_GUESTS_SECTION_HEADER      @"Guests"
#define  EVENT_SETTINGS_SECTION_HEADER    @"Settings"

#define  EVENT_BAR_BUTTON_PUBLISH_TITLE   @"Publish"
#define  EVENT_BAR_BUTTON_EDIT_TITLE      @"Edit"
#define  EVENT_BAR_BUTTON_CLONE_TITLE     @"Clone"

#define EVENT_INFO_SECTION       0
#define RESPONSE_SECTION         1
#define POLL_OPTIONS_SECTION     2
#define EVENT_MESSAGES_SECTION   3
#define EVENT_GUEST_LIST_SECTION 4
#define EVENT_SETTINGS_SECTION   5

#define     EVENT_RESPONSE_SEGMENT_TAG      10
#define     EVENT_SETTINGS_SWITCH_FOR_PUBLIC_GUEST_LIST_TAG    20
#define     EVENT_SETTINGS_SWITCH_FOR_GUESTS_TO_ADD_GUESTS_TAG 30


@interface DisplayEventController() <UITextFieldDelegate, UIActionSheetDelegate, UIPickerViewDelegate>
@property (nonatomic, retain) NSDate *selectedDate;
@property (nonatomic, retain) AbstractActionSheetPicker *actionSheetPicker;
@property (nonatomic) BOOL isInEditMode;
@property (nonatomic) BOOL selfIsOwnerOfThisEvent;
@property (nonatomic) BOOL isCreatingNewEvent;
@property (nonatomic) BOOL isUserSignedIn;
@property (nonatomic) BOOL isPollSectionRequired;
@property (nonatomic) BOOL isGuestListSectionRequired;
@property (nonatomic) BOOL isSettingsSectionRequired;
@property (nonatomic) BOOL isResponseSectionRequired;
@property (nonatomic, retain) NSMutableArray *sectionsInfo;
@property (nonatomic, strong) UITextField *eventTitleTextField;
@property (nonatomic, strong) UITextField *eventStartDateTextField;
@property (nonatomic, strong) UITextField *eventHostNameTextField;
@property (nonatomic, strong) UITextField *eventLocationTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *publishButtonOutlet;
@property (nonatomic, retain) Event* localEvent;
@property (nonatomic, retain) NSManagedObjectContext *scratchPadContext;
           
- (void)setUpSectionHeadersForSections: (int)numSections;
- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element;
@end

@implementation DisplayEventController
@synthesize eventId = _eventId;
@synthesize eventType = _eventType;
@synthesize event = _event;
@synthesize selectedDate = _selectedDate;
@synthesize actionSheetPicker = _actionSheetPicker;
@synthesize isInEditMode = _isInEditMode;
@synthesize selfIsOwnerOfThisEvent = _selfIsOwnerOfThisEvent;
@synthesize isCreatingNewEvent = _isCreatingNewEvent;
@synthesize isUserSignedIn = _isUserSignedIn;
@synthesize isPollSectionRequired = _isPollSectionRequired;
@synthesize isGuestListSectionRequired = _isGuestListSectionRequired;
@synthesize isSettingsSectionRequired = _isSettingsSectionRequired;
@synthesize isResponseSectionRequired = _isResponseSectionRequired;
@synthesize sectionsInfo = _sectionsInfo;
@synthesize eventTitleTextField = _eventTitleTextField;
@synthesize eventHostNameTextField = _eventHostNameTextField;
@synthesize eventLocationTextField = _eventLocationTextField;
@synthesize publishButtonOutlet = _publishButtonOutlet;
@synthesize eventStartDateTextField = _eventStartDateTextField;
@synthesize localEvent = _localEvent;
@synthesize scratchPadContext = _scratchPadContext;
#pragma mark - All Initializations and utilities

- (NSManagedObjectContext *)scratchPadContext
{
    if (!_scratchPadContext) {
        _scratchPadContext = [[NSManagedObjectContext alloc] init];
        [_scratchPadContext setPersistentStoreCoordinator:[RKObjectManager sharedManager].objectStore.persistentStoreCoordinator];
        
    }
    
    return _scratchPadContext;
}

- (void) copyEventsFrom: (Event *)from To: (Event *)to
{
    to.eventName = from.eventName;
    to.eventDescription = from.eventDescription;
    to.eventStartDate = from.eventStartDate;
    to.eventLocation = from.eventLocation;
    to.hostId = from.hostId;
    to.eventType = from.eventType;
    to.isLocallyModified = [NSNumber numberWithInt:1];
    
    NSError *error;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    request.predicate = [NSPredicate predicateWithFormat:@"eventId < 0"];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"eventId" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sd];
    NSArray *events = [self.scratchPadContext executeFetchRequest:request error:&error];
    
    if (![events count]) {
        NSLog(@"No Negatives... so giving -1");
        to.eventId = [NSNumber numberWithInt:-1];
    } else {
        Event *event = [events objectAtIndex:0];
        int i = [event.eventId intValue] - 1;
        to.eventId = [NSNumber numberWithInt:i];
        NSLog(@"Found with Negative: %d", [event.eventId intValue]);
    }
}

- (void) makeEventEditable: (BOOL)isEditable
{
    self.eventTitleTextField.enabled = isEditable;
    self.eventStartDateTextField.enabled = isEditable;
    self.eventLocationTextField.enabled = isEditable;
}

- (void) saveFormData
{
    self.localEvent.eventName = self.eventTitleTextField.text;
    self.localEvent.eventLocation = self.eventLocationTextField.text;
}

- (Event *) localEvent
{
    if (!_localEvent) {
        NSManagedObjectContext *moc = self.scratchPadContext;
        NSError *error;
        
        if (self.event) {
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
            request.predicate = [NSPredicate predicateWithFormat:@"eventId = %@", self.event.eventId];
            NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"eventId" ascending:YES];
            request.sortDescriptors = [NSArray arrayWithObject:sd];
            
            NSArray *events = [moc executeFetchRequest:request error:&error];
            
            if (!events || ([events count] > 1)) {
                NSLog(@"FATAL: Duplicates in the DB *********************************************************************");
            } else if (![events count]) {
                NSLog(@"FATAL: Trying to fetch objects not in Context ****************************************************");
            } else {
                NSLog(@"OK... one entry Found!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                _localEvent = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:moc];
                [self copyEventsFrom:[events lastObject] To:_localEvent];
            }
        } else {
            _localEvent = [NSEntityDescription insertNewObjectForEntityForName:@"Event"
                                                        inManagedObjectContext:moc];
        }
    }
    
    return _localEvent;
}

- (NSMutableArray *) sectionsInfo
{
    if (!_sectionsInfo) {
        _sectionsInfo = [[NSMutableArray alloc] initWithCapacity:NUM_SECTIONS];
    }
    return _sectionsInfo;
}

- (UITextField *)eventTitleTextField
{
    if (!_eventTitleTextField) {
        _eventTitleTextField = [[UITextField alloc]init];
    }
    return _eventTitleTextField;
}

- (UITextField *)eventLocationTextField
{
    if (!_eventLocationTextField) {
        _eventLocationTextField = [[UITextField alloc]init];
    }
    
    return _eventLocationTextField;
}

- (UITextField *)eventStartDateTextField {
    if (!_eventStartDateTextField) {
        _eventStartDateTextField = [[UITextField alloc] init];
    }
    
    return _eventStartDateTextField;
}

- (UITextField *)eventHostNameTextField {
    if (!_eventHostNameTextField) {
        _eventHostNameTextField = [[UITextField alloc] init];
    }
    
    return _eventHostNameTextField;
}

- (void) setPropertiesOfTextField: (UITextField *)tf withText: (NSString *)text AndPlaceHolder: (NSString *)placeholder
{
    tf.placeholder = placeholder;
	tf.text = text;         
	tf.autocorrectionType = UITextAutocorrectionTypeNo;
	tf.autocapitalizationType = UITextAutocapitalizationTypeNone;
	tf.adjustsFontSizeToFitWidth = YES;
	tf.textColor = [UIColor colorWithRed:56.0f/255.0f green:84.0f/255.0f blue:135.0f/255.0f alpha:1.0f];
    tf.frame = CGRectMake(100, 12, 210, 30);
    tf.delegate = self;
    tf.returnKeyType = UIReturnKeyNext;
    tf.keyboardType = UIKeyboardTypeDefault;
    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
}

- (BOOL) isDateBeforeToday: (NSDate *)eventDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *today = [NSDate date];
    
    NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
    
    NSDateComponents *date1Components = [calendar components:comps 
                                                    fromDate: today];
    NSDateComponents *date2Components = [calendar components:comps 
                                                    fromDate: eventDate];
    
    today = [calendar dateFromComponents:date1Components];
    eventDate = [calendar dateFromComponents:date2Components];
    
    NSComparisonResult result = [today compare:eventDate];
    
    if (result == NSOrderedAscending) {
        //Event Date in Future
        return NO;
    } else if (result == NSOrderedDescending) {
        return YES;
    }  else {
        return NO;
    }
}

#pragma mark - Date Picker Related

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    self.localEvent.eventStartDate = selectedDate;
    self.eventStartDateTextField.text = [ActionSheetDatePicker getStringFromDate:selectedDate];
    [self.eventStartDateTextField  resignFirstResponder];
    [self.eventLocationTextField  becomeFirstResponder];
}

- (void) eventDateTouchDownEvent:(id)sender {
    self.actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@""
                                                           datePickerMode:UIDatePickerModeDateAndTime
                                                             selectedDate:self.selectedDate
                                                                   target:self
                                                                   action:@selector(dateWasSelected:element:)
                                                                   origin:sender];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];    
}

#pragma mark - Event Save and Edit Related

- (void) publishEvent
{
    [self saveFormData];
    //Just validate the local event and savecontext on pass....
    
    if (self.event) {
        //Changing an already existing event... Update Coredata and PUT
        
    } else {
        //New one or a cloned one. We dont need to differentiate... Just create a new entry and post.
        //Get the Response back and update the event ID.
        //If server is not reachable... mark it as modified offline.
        NSLog(@"OK Publishing the Event!!!!!!!!!!!!!!!!!!");
        NSError *error;
        
        if (![self.scratchPadContext save:&error])  {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (void) cloneEvent {
    //No Core data involved here. Copy the events from self.event and make it NULL...
    //Update local event entry and treat it as a new one.
    self.publishButtonOutlet.title = EVENT_BAR_BUTTON_PUBLISH_TITLE;
    self.isPollSectionRequired = YES;
    self.isGuestListSectionRequired = YES;
    self.isSettingsSectionRequired = YES;
    self.isCreatingNewEvent = YES;
    self.isResponseSectionRequired = NO;
    self.selfIsOwnerOfThisEvent = YES;
    self.localEvent.eventName = self.event.eventName;
    self.event = nil;
    [self.sectionsInfo removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - All IB Actions

- (IBAction)PublishButtonPressed:(id)sender {
    
    if ([self.publishButtonOutlet.title isEqualToString:EVENT_BAR_BUTTON_PUBLISH_TITLE]) {
        self.publishButtonOutlet.title = EVENT_BAR_BUTTON_EDIT_TITLE;
        NSLog(@"Should Save the event now... ");
        [self publishEvent];
        [self makeEventEditable:FALSE];
        
    } else if ([self.publishButtonOutlet.title isEqualToString:EVENT_BAR_BUTTON_EDIT_TITLE]) {
        self.isInEditMode = YES;
        self.publishButtonOutlet.title = EVENT_BAR_BUTTON_PUBLISH_TITLE;
        [self makeEventEditable:TRUE];
                
        //Make the Poll and Message Cells Editable -- FIXME
        
    } else if ([self.publishButtonOutlet.title isEqualToString:EVENT_BAR_BUTTON_CLONE_TITLE]) {
        [self cloneEvent];        
    }
}

#pragma mark - Target Actions

- (void)segmentAction:(UISegmentedControl *)sender
{
    if (sender.tag == EVENT_RESPONSE_SEGMENT_TAG) {
        //Save the response here to DB and send it to server.
    }
}

- (void)switchAction:(UISwitch*)sender
{
    NSLog(@"switchAction: sender = %d, isOn %d",  [sender tag], [sender isOn]);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.eventStartDateTextField) {
        [self eventDateTouchDownEvent:textField];
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Saving the data!!!!!!!!!!!!!");
    self.localEvent.eventName = self.eventTitleTextField.text;
    self.localEvent.eventLocation = self.eventLocationTextField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.eventLocationTextField) {
        [textField resignFirstResponder];
        return YES;
    }
    
    if (textField == self.eventTitleTextField) {
        [self.eventStartDateTextField becomeFirstResponder];
    } else if (textField == self.eventStartDateTextField) {
        [self.eventLocationTextField becomeFirstResponder];
    }
    
    return YES;
}



#pragma mark - Event Table construction Data

- (int) getPollCount
{
    if (self.localEvent.hasPolling) {
        return 1;
    } else {
        return 1;
    }
}

- (int) getGuestListCount
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numSections = 2;
    
    if (self.isResponseSectionRequired) {
        numSections++;
    }
    
    if (self.isPollSectionRequired) {
        numSections++;
    }
    
    if (self.isGuestListSectionRequired) {
        numSections++;
    }
    
    if (self.isSettingsSectionRequired) {
        numSections++;
    }
    
    [self setUpSectionHeadersForSections:numSections];
    return numSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionHeader = [self.sectionsInfo objectAtIndex:section];
    
    if ([sectionHeader isEqualToString:EVENT_INFO_SECTION_HEADER]) {
        if (self.selfIsOwnerOfThisEvent) {
            return 3;
        } else {
            return 4;
        }
    } else if ([sectionHeader isEqualToString:EVENT_MESSAGES_SECTION_HEADER]) {
        return 1;
    } else if ([sectionHeader isEqualToString:EVENT_RESPONSE_SECTION_HEADER]) {
        return 1;
    } else if ([sectionHeader isEqualToString:EVENT_POLL_SECTION_HEADER]) {
        return [self getPollCount];
    } else if ([sectionHeader isEqualToString:EVENT_GUESTS_SECTION_HEADER]) {
        return [self getGuestListCount];
    } else if ([sectionHeader isEqualToString:EVENT_SETTINGS_SECTION_HEADER]) {
        return 2;
    } else {
        return 1;
    }
        
}

- (void)setUpSectionHeadersForSections: (int)numSections
{
    NSString *sectionHeader;
    int       section = 0;
    
    for (section = 0; section < numSections; section++) {
    
        if (self.selfIsOwnerOfThisEvent) {
            switch (section) {
                case 0:
                    sectionHeader = EVENT_INFO_SECTION_HEADER;
                    break;
                
                case 1:
                    sectionHeader = EVENT_POLL_SECTION_HEADER;
                    break;
                
                case 2:
                    sectionHeader = EVENT_MESSAGES_SECTION_HEADER;
                    break;
                
                case 3:
                    sectionHeader = EVENT_GUESTS_SECTION_HEADER;
                    break;
                
                case 4:
                    sectionHeader = EVENT_SETTINGS_SECTION_HEADER;
                    break;
                
                default:
                    sectionHeader = @"";
                    break;
            }
        } else {
            switch (section) {
                case 0:
                    sectionHeader = EVENT_INFO_SECTION_HEADER;
                    break;
                
                case 1:
                    sectionHeader = EVENT_RESPONSE_SECTION_HEADER;
                    break;
                
                case 2:
                    if (self.isPollSectionRequired) {
                        sectionHeader = EVENT_POLL_SECTION_HEADER;
                    } else {
                        sectionHeader = EVENT_MESSAGES_SECTION_HEADER;
                    }
                    break;
                
                case 3:
                    if (self.isPollSectionRequired) {
                        sectionHeader = EVENT_MESSAGES_SECTION_HEADER;
                    } else {
                        sectionHeader = EVENT_GUESTS_SECTION_HEADER;
                    }
                    break;
                
                default:
                    sectionHeader = @"";
                    break;
            }    
        }
        [self.sectionsInfo insertObject:sectionHeader atIndex:section];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{     
    return [self.sectionsInfo objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    NSString *sectionHeader = [self.sectionsInfo objectAtIndex:indexPath.section];
    BOOL isTextFieldEditable = (self.isCreatingNewEvent || self.isInEditMode)?TRUE:FALSE;
    
    if ([sectionHeader isEqualToString:EVENT_MESSAGES_SECTION_HEADER]) {
        CellIdentifier = @"CellForMessages";
    } else {
        CellIdentifier = @"Cell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    cell.textLabel.text = nil;
    
    if ([cell.contentView subviews]) {
        for (UIView *contentSubView in [cell.contentView subviews]) {
            if ([contentSubView isKindOfClass:[UISegmentedControl class]]) {
                [contentSubView removeFromSuperview];
            }
        }
    }
    
    if ([cell subviews]) {
        for (UIView *subView in [cell subviews]) {
            if ([subView isKindOfClass:[UITextField class]]) {
                [subView removeFromSuperview];
            }
                 
            if ([subView isKindOfClass:[UISwitch class]]) {
                [subView removeFromSuperview];
            }
        }
    }
    
    
    if ([sectionHeader isEqualToString:EVENT_INFO_SECTION_HEADER]) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Title:";
            NSLog(@"Making Text Field with Title: %@ !!!!!!!!!!!!!!!", self.eventTitleTextField.text);
            [self setPropertiesOfTextField:self.eventTitleTextField 
                                  withText:self.localEvent.eventName 
                            AndPlaceHolder:@"Event Title"];
            self.eventTitleTextField.enabled = isTextFieldEditable;
            [cell addSubview:self.eventTitleTextField];
        }
        
        if (indexPath.row == 1) {
            cell.textLabel.text = @"Date:";
            self.eventStartDateTextField.enabled = isTextFieldEditable;
            [self setPropertiesOfTextField:self.eventStartDateTextField
                                  withText:[ActionSheetDatePicker getStringFromDate:self.localEvent.eventStartDate]
                            AndPlaceHolder:@"Event Start Date"];
            [cell addSubview:self.eventStartDateTextField];
        }
        
        if (indexPath.row == 2) {
            cell.textLabel.text = @"Location:";
            self.eventLocationTextField.enabled = isTextFieldEditable;
            [self setPropertiesOfTextField:self.eventLocationTextField
                                  withText:self.localEvent.eventLocation 
                            AndPlaceHolder:@"Event Location"];
            self.eventLocationTextField.returnKeyType = UIReturnKeyDone;
            [cell addSubview:self.eventLocationTextField];
        }
        
        if (indexPath.row == 3) {
            cell.textLabel.text = @"Host:";
            self.eventHostNameTextField.enabled = isTextFieldEditable;
            self.eventHostNameTextField.frame = CGRectMake(100, 12, 210, 30);
            [self setPropertiesOfTextField:self.eventHostNameTextField withText:self.localEvent.hostId AndPlaceHolder:@"Event Host"];
            self.eventHostNameTextField.delegate = self;
            [cell addSubview:self.eventHostNameTextField];
        }
        
       
    } else if ([sectionHeader isEqualToString:EVENT_RESPONSE_SECTION_HEADER]) {
        if (indexPath.row == 0) {
            NSLog(@"Redisplaying the SEGMENTED VIEW????????????????????");
            UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake(-1.0f, -1.0f, 302.0f, 46.0f)];
            segmentedControl.segmentedControlStyle = UISegmentedControlStyleBezeled;
            [segmentedControl insertSegmentWithTitle:@"Yes" atIndex:0 animated:YES];
            [segmentedControl insertSegmentWithTitle:@"No" atIndex:1 animated:NO];
            [segmentedControl insertSegmentWithTitle:@"Maybe" atIndex:2 animated:NO];
            segmentedControl.tag = EVENT_RESPONSE_SEGMENT_TAG;
            [segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
            [cell.contentView addSubview:segmentedControl];
        }
    } else if ([sectionHeader isEqualToString:EVENT_MESSAGES_SECTION_HEADER]) {
        //Make it a button
    } else if ([sectionHeader isEqualToString:EVENT_GUESTS_SECTION_HEADER]) {
        //Make a custom Cell
        NSLog(@"Re-Displaying GUESTS...............");
    } else if ([sectionHeader isEqualToString:EVENT_POLL_SECTION_HEADER]) {
        NSLog(@"Re-Displaying POLLL...............");
        //Make it selectable with a check mark
    } else if ([sectionHeader isEqualToString:EVENT_SETTINGS_SECTION_HEADER]) {
        UISwitch *eventSettingsSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(230, 10, 0, 0)];
        
        eventSettingsSwitch.on = YES;
        [eventSettingsSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Guest List public:";
            eventSettingsSwitch.tag = EVENT_SETTINGS_SWITCH_FOR_PUBLIC_GUEST_LIST_TAG;
        } else {
            cell.textLabel.text = @"Guests can add guests:";
            eventSettingsSwitch.tag = EVENT_SETTINGS_SWITCH_FOR_GUESTS_TO_ADD_GUESTS_TAG;
        }
        
        [cell addSubview:eventSettingsSwitch];
    }
    
    // Configure the cell...
    
    return cell;
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    NSString *userId = [userDefaults getUserLoginName];
    NSNumber *int_val_one = [NSNumber numberWithInt:1];
    
    [super viewDidLoad];
    self.isInEditMode = NO;
    self.navigationItem.title = self.eventType;
    
    if (self.event) {
        /*
         * Displaying an existing Event.
         */
        self.isCreatingNewEvent = NO;
        
        if ([self.event.hostId isEqualToString:userId]) {
            if ([self isDateBeforeToday:self.event.eventStartDate]) {
                self.publishButtonOutlet.title = EVENT_BAR_BUTTON_CLONE_TITLE;
            } else {
                self.publishButtonOutlet.title = EVENT_BAR_BUTTON_EDIT_TITLE;
            }
            
            self.isResponseSectionRequired = NO;
            self.selfIsOwnerOfThisEvent = YES;
            self.isPollSectionRequired = YES;
            self.isGuestListSectionRequired = YES;
            self.isSettingsSectionRequired = YES;
        } else {
            self.publishButtonOutlet.title = EVENT_BAR_BUTTON_CLONE_TITLE;
            self.isSettingsSectionRequired = NO;
            self.isResponseSectionRequired = YES;
            if ([self.event.hasPolling isEqualToNumber:int_val_one]) {
                self.isPollSectionRequired = YES;
            } else {
                self.isPollSectionRequired = NO;
            }
            
            if ([self.event.isGuestListPublic isEqualToNumber:int_val_one]) {
                self.isGuestListSectionRequired = YES;
            } else {
                self.isGuestListSectionRequired = NO;
            }
            self.selfIsOwnerOfThisEvent = NO;
        }
        
    } else {
        /*
         * Creating a new Event.
         */
        self.publishButtonOutlet.title = EVENT_BAR_BUTTON_PUBLISH_TITLE;
        self.isPollSectionRequired = YES;
        self.isGuestListSectionRequired = YES;
        self.isSettingsSectionRequired = YES;
        self.isCreatingNewEvent = YES;
        self.isResponseSectionRequired = NO;
        self.selfIsOwnerOfThisEvent = YES;
        
        self.selectedDate = [NSDate date];
        if (!userId) {
            //Alert the User that the info entered will not be saved.
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login Alert" 
                                                           message:@"Data will be lost. Please Login to Create Events"
                                                          delegate:nil 
                                                 cancelButtonTitle:@"OK" 
                                                 otherButtonTitles:nil, nil];
            
            [alert show];
            self.isUserSignedIn = NO;
        } else {
            self.isUserSignedIn = YES;
        }
    }
}



#pragma mark -- Generic Stuff

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [self setPublishButtonOutlet:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (self.localEvent) {
        [self.scratchPadContext deleteObject:self.localEvent];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
     NSString *sectionHeader = [self.sectionsInfo objectAtIndex:indexPath.section];

     if ([sectionHeader isEqualToString:EVENT_POLL_SECTION_HEADER]) {
         return YES;
     }
     
     if ([sectionHeader isEqualToString:EVENT_GUESTS_SECTION_HEADER]) {
         return YES;
     }
     
     return NO;
 }
 

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



@end
