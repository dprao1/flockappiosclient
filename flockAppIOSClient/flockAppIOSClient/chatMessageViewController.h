//
//  chatMessageViewController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event+Helper.h"

@interface chatMessageViewController : UIViewController

@property (nonatomic, retain) Event *event;

- (id) initWithUser:(NSString *) userName;
@end
