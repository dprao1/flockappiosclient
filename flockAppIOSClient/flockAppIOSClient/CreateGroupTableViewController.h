//
//  CreateGroupTableViewController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Groups.h"
#import "Contacts.h"
#import "ContactsSelector.h"

@interface CreateGroupTableViewController : UITableViewController <UITextFieldDelegate, ContactsSelectorDelegate>
@property (nonatomic, strong) Groups *group;
@property (nonatomic, strong) NSString *groupName;

- (void) doneSelectingWithNumContacts: (NSInteger)numContacts AndContacts: (NSArray *)data;
@end
