//
//  MyGroupsTableViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyGroupsTableViewController.h"
#import "RestKit/RestKit.h"
#import "Groups.h"
#import "CreateGroupTableViewController.h"

@interface MyGroupsTableViewController()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)setupFetchedResultsController;
@end

@implementation MyGroupsTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.fetchedResultsController == nil) {
        [self setupFetchedResultsController];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.fetchedResultsController = nil;
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GroupsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {   
    Groups  *group = (Groups *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [group groupName];
}

- (void)setupFetchedResultsController // attaches an NSFetchRequest to this UITableViewController
{
    if (nil == self.fetchedResultsController) {               
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Groups"];
        request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"groupName" 
                                                                                         ascending:YES 
                                                                                          selector:@selector(localizedCaseInsensitiveCompare:)]];
        
        // no predicate because we want ALL the Topics
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                            managedObjectContext: [[RKObjectManager
                                                                                                    sharedManager].objectStore managedObjectContext]
                                                                              sectionNameKeyPath:nil
                                                                                       cacheName:nil];
        
        
    }
}




- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Groups *group = [self.fetchedResultsController objectAtIndexPath:indexPath];
        NSError *error;
        [[RKObjectManager sharedManager].objectStore.managedObjectContext deleteObject:group];
        if (![[RKObjectManager sharedManager].objectStore.managedObjectContext save:&error]) {
            NSLog(@"Error Deleting the group!!!!!!!!!!!!!");
        }
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
    *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DisplayGroupSegue"]) {
        //if ([segue.destinationViewController respondsToSelector:@selector(setGroup:)]) {
        //    [segue.destinationViewController performSelector:@selector(setGroup:) withObject:group];
        //    return;
        //}
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        Groups *group = [self.fetchedResultsController objectAtIndexPath:indexPath];
        CreateGroupTableViewController * cgc= (CreateGroupTableViewController *) segue.destinationViewController;
        cgc.groupName = group.groupName;
        
    }
}

@end
