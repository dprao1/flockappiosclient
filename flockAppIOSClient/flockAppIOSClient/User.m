//
//  User.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic accountType;
@dynamic dateOfBirth;
@dynamic firstName;
@dynamic homeAddress;
@dynamic isLocallyModified;
@dynamic lastName;
@dynamic password;
@dynamic phoneNumber;
@dynamic userId;

@end
