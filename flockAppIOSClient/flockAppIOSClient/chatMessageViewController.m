//
//  chatMessageViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "chatMessageViewController.h"

@interface chatMessageViewController()
@property (weak, nonatomic) IBOutlet UITextField *chatMessageTextField;
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;

@end

@implementation chatMessageViewController
@synthesize chatMessageTextField = _chatMessageTextField;
@synthesize chatTableView = _chatTableView;
@synthesize event = _event;


- (IBAction)closeButtonPressed:(id)sender {
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)sendButtonPressed:(id)sender {
    
    NSLog(@"Send Button is Pressed ***************");
}

- (id) initWithUser:(NSString *) userName {
    NSLog(@"Initing the Chat view Controller******************");
	if (self = [super init]) {
		
		self.chatMessageTextField.text = userName; // @ missing
	}
	
	return self;
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    NSLog(@"In the View did Load*************");
    [super viewDidLoad];
}


- (void)viewDidUnload
{

    [self setChatMessageTextField:nil];
    [self setChatTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
