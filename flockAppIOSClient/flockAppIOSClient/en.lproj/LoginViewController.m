//
//  LoginViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "FlockApp.h"

@interface LoginViewController() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *LoginButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *FaceBookLoginButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *LogOutButtonOutlet;
@property (weak, nonatomic) IBOutlet UILabel *LoginFailedLabelOutlet;
@end

@implementation LoginViewController

@synthesize userIdTextField = _userIdTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize LoginButtonOutlet = _LoginButtonOutlet;
@synthesize FaceBookLoginButtonOutlet = _FaceBookLoginButtonOutlet;
@synthesize LogOutButtonOutlet = _LogOutButtonOutlet;
@synthesize LoginFailedLabelOutlet = _LoginFailedLabelOutlet;
@synthesize userId = _userId;
@synthesize password = _password;
@synthesize delegate = _delegate;

- (BOOL) loginUser
{
    if ([FlockApp loginWithUser:self.userId password:self.password]) {
        [[self presentingViewController] dismissModalViewControllerAnimated:YES];
    } else {
        self.LoginFailedLabelOutlet.hidden = FALSE;
    }
    return YES;
}

- (IBAction)CancelButtonPressed:(UIBarButtonItem *)sender {
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)LoginButtonPressed:(UIButton *)sender {
    NSString *accountType = sender.titleLabel.text;
    
    NSLog(@"Logging in as %@ user", accountType);
    
    if ([self loginUser]) {
    } else {
        
    }
    //start the spinner
    //Send request to the server.
    //If failed.... say login Failed.....
    //else return to the home page with the user name
}

- (IBAction)LogoutButtonPressed:(UIButton *)sender {
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    self.LoginFailedLabelOutlet.hidden = TRUE;
    if (textField == self.userIdTextField) {
        self.userId = textField.text;
    } else if (textField == self.passwordTextField) {
        self.password = textField.text;
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == self.userIdTextField) {
        //Go to the password Field
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        if ([self loginUser]) {
            
        } else {
            
        }
    }
    return YES;
}


#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userIdTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.LoginFailedLabelOutlet.hidden = TRUE;
    if (self.userId) {
        [self.userIdTextField setText:self.userId];
        //[self.passwordTextField setText:self.password];
        self.FaceBookLoginButtonOutlet.hidden = TRUE; 
        self.LoginButtonOutlet.hidden = TRUE;
    } else {
        self.LogOutButtonOutlet.hidden = TRUE;
    }
}

- (void)viewDidUnload
{
    [self setUserIdTextField:nil];
    [self setPasswordTextField:nil];
    
    [self setLoginButtonOutlet:nil];
    [self setFaceBookLoginButtonOutlet:nil];
    [self setLogOutButtonOutlet:nil];
    [self setLoginFailedLabelOutlet:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
