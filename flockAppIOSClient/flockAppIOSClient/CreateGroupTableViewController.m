//
//  CreateGroupTableViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateGroupTableViewController.h"
#import "Groups.h"
#import "Contacts.h"
#import "RestKit/RestKit.h"

@interface CreateGroupTableViewController()
@property (nonatomic, retain) UITextField *GroupNameTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButtonOutlet;
@property (nonatomic, retain) Groups *localGroup;
@property (nonatomic, retain) NSMutableSet *localNewAddedContacts;
@property (nonatomic, retain) NSManagedObjectContext *scratchPadContext;
@property (nonatomic, retain) NSMutableArray *allContactsData;
@end

@implementation CreateGroupTableViewController 

@synthesize group = _group;
@synthesize groupName = _groupName;
@synthesize GroupNameTextField = _GroupNameTextField;
@synthesize saveButtonOutlet = _saveButtonOutlet;
@synthesize localGroup = _localGroup;
@synthesize localNewAddedContacts = _localNewAddedContacts;
@synthesize scratchPadContext = _scratchPadContext;
@synthesize allContactsData = _allContactsData;

-(NSMutableArray *) allContactsData
{
    if (!_allContactsData) {
        _allContactsData = [[NSMutableArray alloc] init];
    }
    
    return _allContactsData;
}

- (NSManagedObjectContext *)scratchPadContext
{
    if (!_scratchPadContext) {
        _scratchPadContext = [[NSManagedObjectContext alloc] init];
        [_scratchPadContext setPersistentStoreCoordinator:[RKObjectManager sharedManager].objectStore.persistentStoreCoordinator];
        
    }
    
    return _scratchPadContext;
}

- (NSMutableSet *)localNewAddedContacts
{
    if (!_localNewAddedContacts) {
        _localNewAddedContacts = [[NSMutableSet alloc]init];
    }
    
    return _localNewAddedContacts;
}

- (Groups *) fetchGroupFromDBWithGroupName: (NSString *)groupName
{
    NSManagedObjectContext *moc = self.scratchPadContext;
    NSError *error;
    
    if (![groupName length]){
        return NULL;
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Groups"];
    request.predicate = [NSPredicate predicateWithFormat:@"groupName = %@", groupName];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"groupName" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sd];
    
    NSArray *groups = [moc executeFetchRequest:request error:&error];
    
    if (!groups || [groups count] > 1) {
        NSLog(@"Some Error: %@ *************************** either error or multiple groups", error);
        return NULL;
    } else if (![groups count]) {
        return NULL;
    } else {
        return [groups lastObject];
    }
}

- (Contacts *) fetchContactWithName: (NSString *)name AndUserId: (NSString *)userId
{
    NSManagedObjectContext *moc = self.scratchPadContext;
    NSError *error;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Contacts"];
    request.predicate = [NSPredicate predicateWithFormat:@"firstName = %@ AND userId = %@", name, userId];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sd];
    
    NSArray *contacts = [moc executeFetchRequest:request error:&error];
    
    return [contacts lastObject];
}

- (Groups *)localGroup
{    
    if (!_localGroup) {
        if (self.group) {
            NSLog(@"Fetching the Group: %@", self.group.groupName);
            _localGroup = [self fetchGroupFromDBWithGroupName:self.group.groupName];
        } else {
            NSLog(@"Creating an Empty Group");
            _localGroup = [NSEntityDescription insertNewObjectForEntityForName:@"Groups" inManagedObjectContext:self.scratchPadContext];
        }
    }
        
    return _localGroup;
}

-(UITextField *)GroupNameTextField
{
    if (!_GroupNameTextField) {
        _GroupNameTextField = [[UITextField alloc] init];
    }
    
    return _GroupNameTextField;
}

- (IBAction)SaveButtonPressed:(id)sender {
    Groups *group;
    
    [self.GroupNameTextField resignFirstResponder];
    
    if (!self.GroupNameTextField.text.length) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please add title to save the group!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    } else if (!self.group && !self.localNewAddedContacts) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please add contacts to save the group!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    } else if (self.group && (![self.allContactsData count])) {
        NSLog(@"Looks like all contacts are deleted!!!!!!!!!!!!!!!!!!");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please add contacts to save the group!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    }
    
    group = [self fetchGroupFromDBWithGroupName:self.GroupNameTextField.text];
    
    if (group && !self.group) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Group name already exists!!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    }
   
    NSError *error;
    
    self.localGroup.groupName = self.GroupNameTextField.text;
    
    self.localGroup.groupId = [NSNumber numberWithInt:2];
    
    [self.localGroup addGroupContacts:self.localNewAddedContacts];
    
    NSLog(@"adding and saving......");
    
    if (![self.scratchPadContext save:&error]) {
        NSLog(@"Errror saving Group ********************************* %@, %@", error, [error userInfo]);
    }
 
    NSLog(@"OK Saved!!!!!!");
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.groupName) {
        self.group = [self fetchGroupFromDBWithGroupName:self.groupName];
        self.navigationItem.title = self.group.groupName;
        self.allContactsData = [[self.group.groupContacts allObjects] mutableCopy];
        
        if (!self.group) {
            NSLog(@"FATAL.... Cant find a group that exists***************************");
        }
    } else {
        self.navigationItem.title = @"Create Group";
    }
}

- (void)viewDidUnload
{
    [self setGroupNameTextField:nil];
    [self setSaveButtonOutlet:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (_localGroup) {
        NSLog(@"Resetting the Context!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        [self.scratchPadContext reset];
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return ([self.allContactsData count] + 1);
    }
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Group";
    } else {
        return @"Friends in group";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    
    if (indexPath.section == 0) {
        CellIdentifier = @"GroupNameCell";
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            CellIdentifier = @"CellForAddingContacts";
        } else {
            CellIdentifier = @"Cell";
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = @"Title:";
        self.GroupNameTextField.placeholder = @"Group Title";
        self.GroupNameTextField.text = self.group.groupName;         
        self.GroupNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.GroupNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.GroupNameTextField.adjustsFontSizeToFitWidth = YES;
        self.GroupNameTextField.textColor = [UIColor colorWithRed:56.0f/255.0f green:84.0f/255.0f blue:135.0f/255.0f alpha:1.0f];
        self.GroupNameTextField.frame = CGRectMake(75, 12, 210, 30);
        self.GroupNameTextField.delegate = self;
        self.GroupNameTextField.returnKeyType = UIReturnKeyDone;
        self.GroupNameTextField.keyboardType = UIKeyboardTypeDefault;
        self.GroupNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.GroupNameTextField.enabled = TRUE;
        
        [cell addSubview:self.GroupNameTextField];
    } else if (indexPath.section == 1) {
        if (indexPath.row != 0) {
            cell.textLabel.text = [[self.allContactsData objectAtIndex:(indexPath.row - 1)] valueForKey:@"firstName"];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return;
    }
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Contacts *contact = [self.allContactsData objectAtIndex:(indexPath.row - 1)];
        NSLog(@"Deleting the contact..........");
        [self.allContactsData removeObject:contact];
        [self.scratchPadContext deleteObject:[self fetchContactWithName:contact.firstName AndUserId:contact.userId]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return NO;
    } else {
        if (indexPath.row == 0) {
            return NO;
        }
    }
    return YES;
}

- (void) doneSelectingWithNumContacts: (NSInteger)numContacts AndContacts: (NSArray *)data
{   
    
    for (int i = 0; i < [data count]; i++) {
        BOOL insert = YES;
        NSDictionary *obj = (NSDictionary *)[data objectAtIndex:i];
        for (i = 0; i < [self.allContactsData count]; i++) {
            if ([[obj valueForKey:@"email"] isEqualToString:[[self.allContactsData objectAtIndex:i] valueForKey:@"userId"]]) {
                insert = NO;
                break;
            }
        }
                
        
        if (insert == NO) {
            continue;
        }
        
        Contacts *contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contacts" inManagedObjectContext:self.scratchPadContext];        
        
        contact.firstName = [obj valueForKey:@"firstName"];
        contact.userId = [obj valueForKey:@"email"];
        contact.fromAccount = @"local";
        
        [self.localNewAddedContacts addObject:contact];
        [self.allContactsData addObject:contact];
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{      
    [self.GroupNameTextField resignFirstResponder];
    if ([segue.identifier isEqualToString:@"DisplayContactsSegue"]) {
        ContactsSelector *cs = (ContactsSelector *)segue.destinationViewController;
        cs.delegate = self;
    }
}

@end
