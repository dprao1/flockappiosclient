//
//  EventPollOptions.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventPollOptions.h"
#import "EventPoll.h"


@implementation EventPollOptions

@dynamic pollOption;
@dynamic isPartOfEventPoll;

@end
