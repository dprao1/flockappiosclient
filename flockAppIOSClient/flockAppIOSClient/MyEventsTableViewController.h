//
//  MyEventsTableViewController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestKit/RestKit.h"
#import "CoreData/CoreData.h"
#import "CoreDataTableViewController.h"

@interface MyEventsTableViewController : CoreDataTableViewController <RKObjectLoaderDelegate>

@end
