//
//  FlockApp.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlockApp : NSObject

+ (BOOL) loginWithUser: (NSString *)userId password:(NSString *)password;

@end
