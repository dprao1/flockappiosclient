//
//  Groups+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Groups+Helper.h"

@implementation Groups (Helper)
+(RKManagedObjectMapping *)getGroupsClassMappingForObjectManager:(RKObjectManager *)objectManager
{
    RKManagedObjectMapping *groupsMapping = [RKManagedObjectMapping mappingForClass:[Groups class]];
    
    [groupsMapping mapKeyPath:@"groupName"    toAttribute:@"groupName"];
    [groupsMapping mapKeyPath:@"groupId"      toAttribute:@"groupId"];
    
    groupsMapping.primaryKeyAttribute = @"groupName";
    
    [objectManager.mappingProvider setMapping:groupsMapping forKeyPath:@"/groups"];
    
    RKObjectMapping *serializationMapping = [groupsMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[Groups class]];
    
    return groupsMapping;
}
@end
