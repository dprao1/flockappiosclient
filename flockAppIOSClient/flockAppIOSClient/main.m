//
//  main.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "flockAppAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([flockAppAppDelegate class]));
    }
}
