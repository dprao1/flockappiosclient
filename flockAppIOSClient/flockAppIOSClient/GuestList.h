//
//  GuestList.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event;

@interface GuestList : NSManagedObject

@property (nonatomic, retain) NSString * eventResponse;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * pollChoice;
@property (nonatomic, retain) Event *GuestListBelongsTo;

@end
