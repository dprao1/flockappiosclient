//
//  Header.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef flockAppIOSClient_flockAppConstants_h
#define flockAppIOSClient_flockAppConstants_h

#define FLOCK_APP_BASE_URL              @"http://localhost:8080/flockAppServer/rest/json"
#define MIN_PASSWORD_LENGTH             6
#define FOOD_EVENT                      @"Food"
#define MOVIE_EVENT                     @"Movie"
#define HANGOUT_EVENT                   @"Hangout"
#define SPORTS_EVENT                    @"Sports"
#define PARTY_EVENT                     @"Party"
#endif

