//
//  Messages+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Messages+Helper.h"

@implementation Messages (Helper)

+(RKManagedObjectMapping *)getMessagesClassMappingForObjectManager: (RKObjectManager *)objectManager
{
    RKManagedObjectMapping *messagesMapping = [RKManagedObjectMapping mappingForClass:[Messages class]];
    
    [messagesMapping mapKeyPath:@"message"  toAttribute:@"message"];
    [messagesMapping mapKeyPath:@"date"     toAttribute:@"date"];
        
    [objectManager.mappingProvider setMapping:messagesMapping forKeyPath:@"/message"];
    
    RKObjectMapping *serializationMapping = [messagesMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[Messages class]];
    
    [objectManager.router routeClass:[Messages class] toResourcePath:@"/message/:userId/:eventId"];
    
    return messagesMapping;
}
@end
