//
//  UserEventPollChoice.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserEventPollChoice.h"
#import "EventPollOptions.h"
#import "GuestList.h"


@implementation UserEventPollChoice

@dynamic pollChoiceOfUser;
@dynamic pollOptionOfPoll;

@end
