//
//  userDefaults.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userDefaults : NSObject
+ (void) saveUserLoginName: (NSString *)userId;
+ (NSString *) getUserLoginName;

@end
