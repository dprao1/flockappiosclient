//
//  CreateDisplayEventController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CreateDisplayEventController.h"
#import "ActionSheetDatePicker.h"
#import "NSDate+TCUtils.h"
#import "userDefaults.h"

@interface CreateDisplayEventController() <UITextFieldDelegate, UIActionSheetDelegate, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *eventDate;
@property (weak, nonatomic) IBOutlet UITextField *eventTitle;
@property (nonatomic, retain) NSDate *selectedDate;
@property (nonatomic, retain) AbstractActionSheetPicker *actionSheetPicker;
@property (weak, nonatomic) IBOutlet UINavigationItem *eventNavigationBar;

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element;
@end


@implementation CreateDisplayEventController
@synthesize eventDate = _eventDate;
@synthesize eventTitle = _eventTitle;
@synthesize selectedDate = _selectedDate;
@synthesize actionSheetPicker = _actionSheetPicker;
@synthesize eventNavigationBar = _eventNavigationBar;
@synthesize eventId = _eventId;
@synthesize eventType = _eventType;
@synthesize event = _event;

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    self.eventDate.text = [ActionSheetDatePicker getStringFromDate:selectedDate];
}

- (IBAction)eventDateTouchDownEvent:(id)sender {
    self.actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@""
                                                           datePickerMode:UIDatePickerModeDateAndTime
                                                             selectedDate:self.selectedDate
                                                                   target:self
                                                                   action:@selector(dateWasSelected:element:)
                                                                   origin:sender];
        self.actionSheetPicker.hideCancel = YES;
        [self.actionSheetPicker showActionSheetPicker];    
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"In the Delegate begin editing");
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"TextField ended editing");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    NSString *userId = [userDefaults getUserLoginName];

    [super viewDidLoad];
    self.selectedDate = [NSDate date];
    self.eventDate.delegate = self;
    self.eventTitle.delegate = self;
    if (!userId) {
        //Alert the User that the info entered will not be saved.
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login Alert" 
                                                       message:@"Data will be lost. Please Login to Create Events"
                                                      delegate:nil 
                                             cancelButtonTitle:@"OK" 
                                             otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    if (self.event) {
        NSLog(@"Came here to Display Stuff!!!!!!");
        self.eventNavigationBar.title = self.event.eventType;
        
        self.eventTitle.text = self.event.eventName;
        self.eventTitle.userInteractionEnabled = NO;
        
        self.eventDate.text = [ActionSheetDatePicker getStringFromDate:self.event.eventStartDate];
        self.eventDate.userInteractionEnabled = NO;
        
    } else {
        self.eventNavigationBar.title = self.eventType;
    }
}


- (void)viewDidUnload
{
    [self setEventTitle:nil];
    [self setEventDate:nil];
    [self setEventNavigationBar:nil];
    [self setEventTitle:nil];
    [self setEventDate:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
