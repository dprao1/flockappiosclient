//
//  EventPoll+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventPoll.h"
#import "RestKit/RestKit.h"

@interface EventPoll (Helper)
+(RKManagedObjectMapping *)getEventPollClassMappingForObjectManager: (RKObjectManager *)objectManager;
@end
