//
//  flockAppViewController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface flockAppViewController : UIViewController <LoginViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UINavigationItem *HomeNavigationBarOutlet;

@end
