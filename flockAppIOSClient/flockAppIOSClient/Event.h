//
//  Event.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventPoll, GuestList, Messages;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * allowGuestsToAddGuests;
@property (nonatomic, retain) NSString * eventDescription;
@property (nonatomic, retain) NSNumber * eventId;
@property (nonatomic, retain) NSString * eventLocation;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSDate * eventStartDate;
@property (nonatomic, retain) NSString * eventType;
@property (nonatomic, retain) NSNumber * hasPolling;
@property (nonatomic, retain) NSString * hostId;
@property (nonatomic, retain) NSNumber * isGuestListPublic;
@property (nonatomic, retain) NSNumber * isLocallyModified;
@property (nonatomic, retain) NSDate * lastUpdated;
@property (nonatomic, retain) NSSet *EventGuests;
@property (nonatomic, retain) NSSet *eventPolls;
@property (nonatomic, retain) NSSet *hasMessages;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addEventGuestsObject:(GuestList *)value;
- (void)removeEventGuestsObject:(GuestList *)value;
- (void)addEventGuests:(NSSet *)values;
- (void)removeEventGuests:(NSSet *)values;

- (void)addEventPollsObject:(EventPoll *)value;
- (void)removeEventPollsObject:(EventPoll *)value;
- (void)addEventPolls:(NSSet *)values;
- (void)removeEventPolls:(NSSet *)values;

- (void)addHasMessagesObject:(Messages *)value;
- (void)removeHasMessagesObject:(Messages *)value;
- (void)addHasMessages:(NSSet *)values;
- (void)removeHasMessages:(NSSet *)values;

@end
