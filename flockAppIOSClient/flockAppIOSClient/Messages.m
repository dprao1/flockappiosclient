//
//  Messages.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Messages.h"
#import "Event.h"


@implementation Messages

@dynamic message;
@dynamic date;
@dynamic messageId;
@dynamic userId;
@dynamic belongsToEvent;

@end
