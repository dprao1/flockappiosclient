//
//  User+Helper.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "User.h"
#import "RestKit/RestKit.h"

@interface User (Helper)
+(RKManagedObjectMapping *)getUserClassMappingForObjectManager: (RKObjectManager *)objectManager;
@end
