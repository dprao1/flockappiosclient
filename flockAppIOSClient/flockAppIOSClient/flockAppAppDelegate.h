//
//  flockAppAppDelegate.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface flockAppAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
