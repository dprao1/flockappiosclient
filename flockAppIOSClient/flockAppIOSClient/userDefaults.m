//
//  userDefaults.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "userDefaults.h"

@implementation userDefaults

#define USER_ID_STRING     @"UserLoggedIn"

+ (void) saveUserLoginName: (NSString *)userId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:userId forKey:USER_ID_STRING];
    [defaults synchronize];
}

+ (NSString *) getUserLoginName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return ([defaults stringForKey:USER_ID_STRING]);
}
@end
