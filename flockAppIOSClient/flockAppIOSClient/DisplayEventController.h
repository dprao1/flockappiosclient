//
//  DisplayEventController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event+Helper.h"

@interface DisplayEventController : UITableViewController <UITextFieldDelegate>
@property (nonatomic, copy) NSNumber   *eventId;
@property (nonatomic, copy) NSString   *eventType;
@property (nonatomic, strong) Event    *event;
@end
