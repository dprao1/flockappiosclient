//
//  UserEventPollChoice.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventPollOptions, GuestList;

@interface UserEventPollChoice : NSManagedObject

@property (nonatomic, retain) GuestList *pollChoiceOfUser;
@property (nonatomic, retain) EventPollOptions *pollOptionOfPoll;

@end
