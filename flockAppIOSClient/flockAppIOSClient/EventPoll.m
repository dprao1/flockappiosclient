//
//  EventPoll.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 2/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EventPoll.h"
#import "Event.h"
#import "EventPollOptions.h"


@implementation EventPoll

@dynamic chosenOption;
@dynamic isOpen;
@dynamic pollDescription;
@dynamic belongsToEvent;
@dynamic optionsMapping;

@end
