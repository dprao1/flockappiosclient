//
//  Event.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Event.h"
#import "EventPoll.h"
#import "GuestList.h"
#import "Messages.h"


@implementation Event

@dynamic allowGuestsToAddGuests;
@dynamic eventDescription;
@dynamic eventId;
@dynamic eventLocation;
@dynamic eventName;
@dynamic eventStartDate;
@dynamic eventType;
@dynamic hasPolling;
@dynamic hostId;
@dynamic isGuestListPublic;
@dynamic isLocallyModified;
@dynamic lastUpdated;
@dynamic EventGuests;
@dynamic eventPolls;
@dynamic hasMessages;

@end
