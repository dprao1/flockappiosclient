//
//  Event+Helper.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Event+Helper.h"

@implementation Event (Helper)

+(RKManagedObjectMapping *)getEventClassMappingForObjectManager:(RKObjectManager *)objectManager
{
    RKManagedObjectMapping *eventMapping = [RKManagedObjectMapping mappingForClass:[Event class]];
    
    [eventMapping mapKeyPath:@"eventName"               toAttribute:@"eventName"];
    [eventMapping mapKeyPath:@"hostId"                  toAttribute:@"hostId"];
    [eventMapping mapKeyPath:@"eventStartAt"            toAttribute:@"eventStartDate"];
    [eventMapping mapKeyPath:@"eventLocation"           toAttribute:@"eventLocation"];
    [eventMapping mapKeyPath:@"eventType"               toAttribute:@"eventType"];
    [eventMapping mapKeyPath:@"hasPolling"              toAttribute:@"hasPolling"];
    [eventMapping mapKeyPath:@"isGuestListPublic"       toAttribute:@"isGuestListPublic"];
    [eventMapping mapKeyPath:@"allowGuestsToAddGuests"  toAttribute:@"allowGuestsToAddGuests"];
    [eventMapping mapKeyPath:@"eventDescription"        toAttribute:@"eventDescription"];
    [eventMapping mapKeyPath:@"eventId"                 toAttribute:@"eventId"];
    [eventMapping mapKeyPath:@"lastUpdated"             toAttribute:@"lastUpdated"];
    
    eventMapping.primaryKeyAttribute = @"eventId";
    
    [objectManager.mappingProvider setMapping:eventMapping forKeyPath:@"/event"];
    
    RKObjectMapping *serializationMapping = [eventMapping inverseMapping];
    [objectManager.mappingProvider setSerializationMapping:serializationMapping forClass:[Event class]];
    
    [objectManager.router routeClass:[Event class] toResourcePath:@"/event/:eventId"];
    [objectManager.router routeClass:[Event class] toResourcePath:@"/event" forMethod:RKRequestMethodPOST];
    
    return eventMapping;
}

@end
