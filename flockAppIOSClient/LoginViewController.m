//
//  LoginViewController.m
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "FlockApp.h"
#include "flockAppConstants.h"

@interface LoginViewController() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *LoginButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *FaceBookLoginButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *LogOutButtonOutlet;
@property (weak, nonatomic) IBOutlet UILabel *LoginFailedLabelOutlet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoginSpinnerOutlet;
@end

@implementation LoginViewController

@synthesize userIdTextField = _userIdTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize LoginButtonOutlet = _LoginButtonOutlet;
@synthesize FaceBookLoginButtonOutlet = _FaceBookLoginButtonOutlet;
@synthesize LogOutButtonOutlet = _LogOutButtonOutlet;
@synthesize LoginFailedLabelOutlet = _LoginFailedLabelOutlet;
@synthesize LoginSpinnerOutlet = _LoginSpinnerOutlet;
@synthesize userId = _userId;
@synthesize password = _password;
@synthesize delegate = _delegate;

- (BOOL) loginUser
{
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    //Do the UserId and password validation
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:self.userId];
    
    if (!myStringMatchesRegEx) {
        return NO;
    }
    
    if ([self.password length] < MIN_PASSWORD_LENGTH) {
        return NO;
    }
    
    if ([FlockApp loginWithUser:self.userId password:self.password]) {
        return YES;
    } else {
        return NO;
    }
}

- (IBAction)CancelButtonPressed:(UIBarButtonItem *)sender {
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}

- (IBAction)LoginButtonPressed:(UIButton *)sender {
    NSString *accountType = sender.titleLabel.text;
    
    [self.LoginSpinnerOutlet startAnimating];
    
    NSLog(@"Logging in as %@ user", accountType);
    
    if ([self loginUser]) {
        
        [self.delegate userLoggedIn:self withLoginName:self.userId   
                 Password:self.password
           andAccountType:accountType];
        
        [[self presentingViewController] dismissModalViewControllerAnimated:YES];
        
    } else {
        self.LoginFailedLabelOutlet.hidden = FALSE;
    }
    //start the spinner
    //Send request to the server.
    //If failed.... say login Failed.....
    //else return to the home page with the user name
    
    [self.LoginSpinnerOutlet stopAnimating];
}

- (IBAction)LogoutButtonPressed:(UIButton *)sender {
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    self.LoginFailedLabelOutlet.hidden = TRUE;
    if (textField == self.userIdTextField) {
        self.userId = textField.text;
    } else if (textField == self.passwordTextField) {
        self.password = textField.text;
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == self.userIdTextField) {
        //Go to the password Field
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        //Dismiss the Keyboard here.
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userIdTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.LoginFailedLabelOutlet.hidden = TRUE;
    self.LoginSpinnerOutlet.hidesWhenStopped = YES;
    if (self.userId) {
        [self.userIdTextField setText:self.userId];
        [self.passwordTextField setText:self.password];
        self.FaceBookLoginButtonOutlet.hidden = TRUE; 
        self.LoginButtonOutlet.hidden = TRUE;
    } else {
        self.LogOutButtonOutlet.hidden = TRUE;
    }
}

- (void)viewDidUnload
{
    [self setUserIdTextField:nil];
    [self setPasswordTextField:nil];
    
    [self setLoginButtonOutlet:nil];
    [self setFaceBookLoginButtonOutlet:nil];
    [self setLogOutButtonOutlet:nil];
    [self setLoginFailedLabelOutlet:nil];
    [self setLoginSpinnerOutlet:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
