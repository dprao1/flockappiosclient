//
//  LoginViewController.h
//  flockAppIOSClient
//
//  Created by Kiran Tatiparthi on 1/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginViewController;

@protocol LoginViewControllerDelegate <NSObject>

- (void) userLoggedIn:(LoginViewController *)sender
        withLoginName: (NSString *)userId
             Password: (NSString *)password
       andAccountType: (NSString *)accountType;

@end

@interface LoginViewController : UIViewController
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *password;

@property (nonatomic, weak) id <LoginViewControllerDelegate> delegate;
@end

